# A semantics of Core Erlang with handling of signals
## Presentation
This project is a work in progress, pushed now to be able to point at it in related papers. The repository is currently empty, but will be filled during its progress. 
We are proposing a small step semantics focused on the order of messages. It models signal handling and come monitoring behaviors, but presents simplifications: its spawn never fails, it cannot represent continuous time, and nodes, try-catch expressions, or the ability to dynamically update code are not modeled.

## Future updates
You will ultimately find in this repository :
- a technical report describing our semantics,
- an implementation in Maude.